'use strict';

/*
 * @name shopModule
 * @description: Modulo para listar, filtrar, buscar y ver detalles de los productos 
 */
angular.module('shopModule', ['ui.bootstrap'])
   .config(function ($routeProvider, $httpProvider) {
    $routeProvider    
      .when('/shopList', {
        templateUrl: 'views/shop/shopList.html',
        controller:  'shopListCtrl',
        controllerAs: 'shop' 
      })
      .when('/cartList', {
        templateUrl: 'views/shop/cartList.html',
        controller:  'cartListCtrl',
        controllerAs: 'cart' 
      })              
      .otherwise({
        redirectTo: '/'
      });
  });
