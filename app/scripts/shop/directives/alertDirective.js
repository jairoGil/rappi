'use strict';

/**
 * @ngdoc function
 * @description: Esta directiva genera un mensaje de alerta que puede ser de éxito, 
                 advertencia error, importante  
 */
 angular.module('shopModule')      
  .directive('alertDirective', 

        function  alertDirective () {   
        return {
            restrict : 'E',
            scope : {
                message : "=",
                type : "=",
                show : "="
            },
            controller : function ($scope) {
                $scope.closeAlert = function () {
                    $scope.show = false;
                    $scope.type = "alert alert-dismissable alert-";
                    $scope.message = "";
                };

                $scope.getStrongMessage = function (type) {
                    var strongMessage = "";
                    if(type == "success"){
                        strongMessage = "\xC9xito ! ";
                    } else if(type == "warning"){
                        strongMessage = "Cuidado ! ";
                    } else if(type == "danger"){
                        strongMessage = "Error ! ";
                    } else if(type == "info"){
                        strongMessage = "Importante ! ";
                    }
                    return strongMessage;
                }
            },
            templateUrl : "views/util/alert.html"
        };
    });
