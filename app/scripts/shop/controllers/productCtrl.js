'use strict';

/**
 * @ngdoc function
 * @name shopModule
 * @description Controlador para el POP-UP con el detalle de un producto
 */
angular.module('shopModule')
  .controller('productCtrl', ['$scope', 'product',  productCtrl]);

function productCtrl ($scope, product) {
    $scope.data        = {};
    $scope.info        = {};
     

  initCtrl();

  /*
  * Funcion de inicio del controlador
  */
  function initCtrl () {
    console.log("ENTRE");
    console.log(product);
    $scope.data.product = product;
  }
       
}
