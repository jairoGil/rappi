'use strict';

/*
 * @ngdoc function
 * @name cartListCtrl
 * @description
 * Controlador para listar y filtrar articulos
 */
angular.module('shopModule')
  .controller('cartListCtrl', ['$scope', 'cartService', cartListCtrl]);

function cartListCtrl ($scope, cartService) {

  $scope.data = {};
  $scope.info = {}; 
    

  initCtrl();

  //Funcion de inicio del controlador, 
  function initCtrl () {
    $scope.data.products = cartService.cartList();
  } 


  //Elimina un producto de la lista del carrito de compra
  this.removeProduct = function removeProduct(product) {
    $scope.data.products = cartService.removeCart(product.id);
    $scope.info.alert = {
      message: "INFO-DELETE-TO-CART",
      type: "success",
      show : true
    };

  }
}