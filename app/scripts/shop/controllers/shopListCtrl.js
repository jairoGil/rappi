'use strict';

/**
 * @ngdoc function
 * @name demosApp.controller:shopListCtrl
 * @description
 * Controlador para listar y filtrar articulos
 */
angular.module('shopModule')
  .controller('shopListCtrl', ['$scope', 'shopRESTService', '$uibModal', 'cartService',  shopListCtrl]);

function shopListCtrl ($scope, shopRESTService, $uibModal, cartService) {

  $scope.data = {};
  $scope.info = {};

  $scope.info.sortType     = 'name'; // Columna de ordenamiento por defecto Nombre
  $scope.info.sortReverse  = false;  // Ordenamiento invertido (Mayor - Menor) por defecto No
  $scope.info.search       = '';     // Busqueda 
  $scope.info.leastPrice   = null;   // Precio mínimo
  $scope.info.mostPrice    = null;   // Precio máximo
  $scope.info.available    = null;  // Disponible
  $scope.info.bestseller   = null;  // Mas vendidos
  $scope.info.categories   = [];     // Categorias   
    

  initCtrl();

  /*
  * Funcion de inicio del controlador, trae la data de productos
  */
  function initCtrl () {
    shopRESTService.getAllLists().then(
      function (list) {
        $scope.data.products = list.products;
        $scope.data.categories = list.categories;

    })
    .catch(function  (error) {
      $scope.info.alert = {
        message: "ERROR-LOADING",
        type: "danger",
        show : true
      };
    })    
  } 


  /*
   * @description: filtra los productos por precio minino y precio maximo
   */
  this.priceFilter =  function priceFilter(product) {
    var price = parseInt(product.price.replace(/\D/g,''));
    return (($scope.info.leastPrice == null || price > $scope.info.leastPrice) && ($scope.info.mostPrice == null || price < $scope.info.mostPrice));
  }
  
  /*
   * @description: filtra los productos por disponibilidad
   */
  this.availableFilter =  function availableFilter(product) {
    return ($scope.info.available == null || $scope.info.available == product.available);
  }

  /*
   * @description: filtra los productos por disponibilidad
   */
  this.bestsellerFilter =  function bestsellerFilter(product) {
    return ($scope.info.bestseller  == null || $scope.info.bestseller == product.best_seller);
  }

  /*
   * @description: filtra los productos por su categoria
   */
  this.categoriesFilter =  function categoriesFilter(product) {
    var productCategories = new Set(product.categories), 
        searchCategories  = new Set($scope.info.categories),
        merge = new Set(function*() {  yield* productCategories; yield* searchCategories; }());
    console.log($scope.info.categories, "las categorias", $scope.info.categories == [] )
    return ($scope.info.categories.length == 0 || merge.size < productCategories.size + searchCategories.size);
  }


  /*
   * @description: Pop-up con los detalles de producto
   */
  this.modalProduct = function modalProduct (product){
    var modalInstance = $uibModal.open({
      animation: this.animationsEnabled,
      templateUrl: 'views/shop/productModal.html',
      controller: 'productCtrl',
      controllerAs: 'product',
      size: 'md',
      resolve: {
        product: function () {
          return product;
        }
      }      
    });
  }

  //Añade un elemento al carrito de compras
  this.addToCart = function addToCart (product) {
    var result =cartService.addToCart(product);
    if(result >= 0){
      $scope.info.alert = {
        message: "INFO-ADD-TO-CART",
        type: "success",
        show : true
      };
    }else{
      $scope.info.alert = {
        message: "INFO-ALREADY-EXIST",
        type: "info",
        show : true
      };
    }
  }

  //Reinicia los criterios de filtros
  this.clear = function clear() {
    $scope.info.search       = '';     // Busqueda 
    $scope.info.leastPrice   = null;   // Precio mínimo
    $scope.info.mostPrice    = null;   // Precio máximo
    $scope.info.available    = null;  // Disponible
    $scope.info.bestseller   = null;  // Mas vendidos
    $scope.info.categories   = [];     // Categorias  
  }
}