'use strict';

/**
 * @ngdoc function
 * @name cartService
 * @description: Mock para mantener global los roductos en el carrito de compras 
 */
angular.module('shopModule')
  .service('cartService', 

function  cartService ($resource) {   
    
  var cart = []
  
  //Encuentra un producto por su Id
  function findProductById (product_id) {
    var index = -1;
    for (var i = 0; i < cart.length; i++) {
      if (cart[i].id === product_id) {
        index = i;
      }
    }
    return index;
  }

  return {

    //Lista los productos en el carrito de compras
    cartList : function () {
      return cart;
    },

    //Añade un producto al carrito de compras
    addToCart : function (product) {
      var index = -1;
      if (findProductById(product.id) == -1) index = cart.push(product)
      return index;
    },

    //Añade un producto al carrito de compras
    removeCart : function (product_id) {
      if (findProductById(product_id) >= 0) cart.splice(findProductById(product_id) , 1) 
      return cart;
    } 

  }

});
