'use strict';

/**
 * @ngdoc function
 * @name shopRESTService
 * @description: Servicios para traer la información desde un archivo local, 
 *               o desde un servicio web
 */
angular.module('shopModule')
  .service('shopRESTService', 

function  shopRESTService ($resource, generalRESTService) {   
    
  var localPath = generalRESTService.getSelectedPath();
  var resource = $resource(localPath);
  
  return {

    //Devuelve una lista de categorias y productos
    getAllLists : function () {
      var auxResource = $resource(localPath, null, {
                                      'query': { method:'GET', isArray: false }  }
                                  );
      return auxResource.query().$promise;
    }                      
  }
});