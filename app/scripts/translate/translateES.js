'use strict';

/*
 * @name translate
 * @description: Mensajes en español de la aplicación
 */
angular.module('translateModule', ['pascalprecht.translate'])
  .config(['$translateProvider', 


function translate ($translateProvider) {   
    
  $translateProvider.translations('es', {

    "SEARCH":               "Buscar",
    "ACCEPT":               "Aceptar",
    "CANCEL":               "Cancelar",
    "SAVE":                 "Guardar",                
    "NEW":                  "Nuevo",
    "EDIT":                 "Editar",
    "DELETE":               "Elimiar",
    "ADD":                  "Añadir",
    "SEND":                 "Enviar",
    "REFRESH":              "Refrescar",
    "TAGS":                 "Etiquetas",
    "CLEAR":                "Limpiar",
    "DETAIL":               "Detalles",
    "OPTIONS":              "Opciones",
    "FILTERS":              "Filtros",

    "NAME":                 "Nombre", 
    "PRICE":                "Precio",   
    "AVAILABLE":            "Disponible",  
    "BESTSELLER":          "Mas vendido",
    "OUT-OF-STOCK":         "Agotado",
    "CATEGORIES":           "Categorias", 

    "LEAST-PRICE":          "Precio mínimo",
    "MOST-PRICE":           "Precio máximo", 

    "SHOP":                 "Tienda",
    "CART":                 "Carrito de compras",
    "ADD-TO-CART":          "Añadir al carrito", 
    "MY-CART":              "Mi Carrito",
    "US-PRODUCTS":          "Nuestros productos",

    "true":                 "Si",
    "false":                "No",

    "lunch":                "Almuerzos",         
    "drinks":               "Bebidas",
    "food":                 "Comida",
    "sea":                  "De mar",

    "ERROR-LOADING":        "Error al cargar las imagenes favor contacte al administrador",
    "INFO-ADD-TO-CART":     "El producto se a añadido a tu carrito de compras",
    "INFO-ALREADY-EXIST":   "El producto ya se encuentra en tu carrito de compras",
    "INFO-DELETE-TO-CART":  "El producto se a eliminado de tu carrito de compras",

    "FANTASTIC":            "Fantastico!",
    "TEST-RAPPI":           "Prueba de ingreso RAPPI",
    "ABOUT-FRAMEWORK":      "Sobre el Framework",
    "FRAMEWORK-DETAIL":     "Este proyecto fue desarrollado en AngularJS, usando el generador yo angular generator version 0.15.1, para compilar una vista previa del mismo se puede usar grunt con el comando grunt serve",
    "ABOUT-PROJECT":        "Sobre el proyecto",
    "ABOUT-URL":            "Sobre el origen de los datos",
    "PROJECT-DETAIL":       "El ordenamiento de los productos por nombre y precio se logra dando click sobre el encabezado de la tabla, o doble click para inverti el orden",
    "CONFIGURE-URL":        "Para modificar el origen de los datos del archivo local a una URL, en main, services, generalRESTServices se puede modificar la constante remotePath y selected para definir de que sitio consumir la información",
    "DEVELOPED-BY":         "Desarrollado por Jairo Gil Todos los derechos reservados"
  });
 
  //Idioma por defecto a mostrar en la aplicación
  $translateProvider.preferredLanguage('es');
  $translateProvider.useSanitizeValueStrategy('escape');  // prevenir errores de seguridad

}]);



