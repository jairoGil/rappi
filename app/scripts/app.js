'use strict';

/**
 * @ngdoc overview
 * @name rappiApp
 * @description
 * # rappiApp
 *
 * Main module of the application.
 */
angular
  .module('rappiApp', [
    'ngAnimate',
    'ngCookies',
    'ngResource',
    'ngRoute',
    'ngSanitize',
    'ngTouch',
    'pascalprecht.translate',    
    'shopModule',
    'translateModule'
  ])
  .config(function ($routeProvider) {
    $routeProvider
      .when('/', {
        templateUrl: 'views/main.html',
        controller: 'MainCtrl',
        controllerAs: 'main'
      })
      .when('/ja', {
        templateUrl: 'views/shop/shopList.html',
        controller: 'MainCtrl',
        controllerAs: 'main'
      })      
      .otherwise({
        redirectTo: '/'
      });
  });
