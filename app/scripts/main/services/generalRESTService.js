'use strict';

/*
 * @name generalRESTService
 * @description: Servicio globlal para consumir servicio JSON desde una URL o desde una ruta local 
 */

angular.module('rappiApp')
  .constant('remotePath','https://remotepath.com/data/1')
  .constant('localPath'  ,'local/data.json')            
  .service('generalRESTService', 

function  generalRESTService (remotePath, localPath) {  

  //var selected = remotePath;  para acceder a los datos por URL
  var selected = localPath;
   
  return {
    
    //Retorna la ruta por defecto de donde traer la data
    getSelectedPath : function () {
      return  selected;
    }

  }

});