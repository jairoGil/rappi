'use strict';

/**
 * @ngdoc function
 * @name rappiApp.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of the rappiApp
 */
angular.module('rappiApp')
  .controller('MainCtrl', function () {
    this.awesomeThings = [
      'HTML5 Boilerplate',
      'AngularJS',
      'Karma'
    ];
  });
